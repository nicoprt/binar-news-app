import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons';
import News from './screens/News';
import Search from './screens/Search';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
    return(
        <Tab.Navigator>
            <Tab.Screen
                name='Home'
                component={News}
                options={{
                    tabBarLabel:'Home',
                    tabBarIcon:({color,size}) => (
                        <MaterialCommunityIcons
                            name='home'
                            color={color}
                            size={size}
                        />
                    ),
                }}
            />

            <Tab.Screen
                name='Search'
                component={Search}
                options={{
                    tabBarLabel:'Search',
                    tabBarIcon:({color,size}) => (
                        <MaterialCommunityIcons
                            name='search'
                            color={color}
                            size={size}
                        />
                    ),
                }}
            />
        </Tab.Navigator>
    );
};

const Router = () => {
    return(
        <Stack.Navigator headerMode='none'>
            <Stack.Screen name='MainApp' component={MainApp}/>
        </Stack.Navigator>
    );
};

export default Router;