import React from 'react';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { NavigationContainer } from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import News from './screens/News';
import Router from './Router';

// const stackNavigator = createStackNavigator({
//   'Binar X Telkomsel News': News
// })

// const App = createAppContainer(stackNavigator)

const App = () => {
  return(
    <NavigationContainer>
      <Router/>
    </NavigationContainer>
  )
}

export default App;