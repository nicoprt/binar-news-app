import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Text, Button, FlatList} from 'react-native';
import Config from 'react-native-config';
import {WebView} from 'react-native-webview';

import NewsCard from '../components/NewsCard';
import NewsApi from '../api/NewsApi';

const News = ({navigation}) => {

    const [news, setNews] = useState([])

    useEffect(()=>{
        getNewsFromAPI()
    }, [])

    // const newsResponse = async() =>{
    //     const response = await NewsApi.get('top-headlines?country=id&apiKey=f5bcab0087914b1cb5be291e30f7f052')
    //     console.log(response.data)
    // }

    function getNewsFromAPI(){
        NewsApi.get(`top-headlines?country=id&apiKey=${Config.API_KEY}&page=1`)
        .then(async function(response){
            setNews(response.data)
        })
        .catch(function(error){
            console.log(error)
        })
    }

    if (!news) {
        return null
    }

    return(
        <View>
            <FlatList 
            data = {news.articles}
            keyExtractor = {(item, index) => 'key' + index} 
            renderItem = {({item}) => {
                return <NewsCard item = {item}/>
            }}/>
        </View>
    )
}

export default News;