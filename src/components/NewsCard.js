import React from 'react';
import {View, StyleSheet, Text, Image, Dimensions, TouchableOpacity} from 'react-native';
import {WebView} from 'react-native-webview';

const {width, height} = Dimensions.get('window')

const NewsCard = ({item}) => {
    return(
        <TouchableOpacity onPress={item.url}>
            <View style={styles.cardView}>
                <Text style={styles.title}>{item.title}</Text>
                <Text style={styles.author}>{item.author}</Text>
                <Text style={styles.publishedAt}>{item.publishedAt}</Text>
                <Image style={styles.image} source= {{uri: item.urlToImage}}/>
                <Text style={styles.description}>{item.description}</Text>
            </View>
        </TouchableOpacity>
    )
}


const styles = StyleSheet.create({
    cardView: {
        backgroundColor:'white',
        margin: width * 0.03,
        borderRadius: width * 0.05,
        shadowColor: 'black',
        shadowOffset: {width: 0.5, height: 0.5},
        shadowOpacity: 0.5,
        shadowRadius: 3
    },
    title: {
        marginHorizontal: width * 0.05,
        marginVertical: width * 0.03,
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold'
    },
    author: {
        marginBottom: width * 0.01,
        marginHorizontal: width * 0.05,
        fontSize: 15,
        color: 'gray'
    },
    publishedAt: {
        marginBottom: width * 0.01,
        marginHorizontal: width * 0.05,
        fontSize: 15,
        color: 'gray'
    },
    image: {
        height: height / 4,
        marginLeft: width * 0.05,
        marginRight: width * 0.05,
        marginVertical: height * 0.02
    },
    description: {
        marginVertical: width * 0.05,
        marginHorizontal: width * 0.02,
        color: 'gray',
        fontSize: 16
    }
})

export default NewsCard;